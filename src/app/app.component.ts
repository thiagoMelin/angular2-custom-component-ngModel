import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
              <form>
                <custom-form [(ngModel)]="dadosEnvio" name="dadosEnvio"></custom-form>
              </form>
              <br/>
              <br/>
              <div style="clear:both">Valor da variável {{dadosEnvio | json}}</div>
            `
})
export class AppComponent {}
