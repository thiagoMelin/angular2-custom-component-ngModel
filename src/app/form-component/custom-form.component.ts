import { Component,  ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, NgModel } from '@angular/forms';
import { ValueAccessorBase } from './value-accessor';

@Component({
    selector: 'custom-form',
    templateUrl: './custom-form.component.html',
    providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: CustomFormComponent,
    multi: true,
  }],
})
export class CustomFormComponent extends ValueAccessorBase<DadosEnvio> {
    @ViewChild(NgModel) dadosEnvio: NgModel;

    handleInput(prop, value) {
        if(!this.value){
            this.value = new DadosEnvio();
        }
        this.value[prop] = value;
        this.value = { ...this.value };
    }

}
export class DadosEnvio {
    nome: string;
    email: string;
    telefone: string;

    constructor(){}
}

